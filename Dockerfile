# Use the official Node.js 18 image as the base image
FROM node:16

# Set the working directory inside the container
WORKDIR /app

# Copy package.json and package-lock.json (if available)
COPY package*.json ./

# Install application dependencies
RUN npm install

# Copy all application files to the container
COPY . .

# Expose any necessary ports (if your app uses any)
# EXPOSE 3000

# Define the command to run your application
CMD ["npm", "start"]
